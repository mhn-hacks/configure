#!/bin/bash -e


# Digikam

echo -n "Applying digikam config..."

kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readCommentNamespaces" --group "Exif.Image.ImageDescription"     --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readCommentNamespaces" --group "Iptc.Application2.Caption"       --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readCommentNamespaces" --group "JPEG/TIFF Comments"              --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readCommentNamespaces" --group "Xmp.acdsee.notes"                --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readCommentNamespaces" --group "Xmp.dc.description"              --key "isDisabled" false
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readCommentNamespaces" --group "Xmp.exif.UserComment"            --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readCommentNamespaces" --group "Xmp.tiff.ImageDescription"       --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readRatingNamespaces" --group "Exif.Image.0x4746"                --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readRatingNamespaces" --group "Exif.Image.0x4749"                --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readRatingNamespaces" --group "Iptc.Application2.Urgency"        --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readRatingNamespaces" --group "Xmp.MicrosoftPhoto.Rating"        --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readRatingNamespaces" --group "Xmp.acdsee.rating"                --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readRatingNamespaces" --group "Xmp.xmp.Rating"                   --key "isDisabled" false
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readTagsNamespaces" --group "Exif.Image.XPKeywords"              --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readTagsNamespaces" --group "Iptc.Application2.Keywords"         --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readTagsNamespaces" --group "Xmp.MicrosoftPhoto.LastKeywordXMP"  --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readTagsNamespaces" --group "Xmp.acdsee.categories"              --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readTagsNamespaces" --group "Xmp.dc.subject"                     --key "isDisabled" false
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readTagsNamespaces" --group "Xmp.digiKam.TagsList"               --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readTagsNamespaces" --group "Xmp.lr.hierarchicalSubject"         --key "isDisabled" false
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "readTagsNamespaces" --group "Xmp.mediapro.CatalogSets"           --key "isDisabled" true

kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeCommentNamespaces" --group "Exif.Image.ImageDescription"    --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeCommentNamespaces" --group "Iptc.Application2.Caption"      --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeCommentNamespaces" --group "JPEG/TIFF Comments"             --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeCommentNamespaces" --group "Xmp.acdsee.notes"               --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeCommentNamespaces" --group "Xmp.dc.description"             --key "isDisabled" false
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeCommentNamespaces" --group "Xmp.exif.UserComment"           --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeCommentNamespaces" --group "Xmp.tiff.ImageDescription"      --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeRatingNamespaces" --group "Exif.Image.0x4746"               --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeRatingNamespaces" --group "Exif.Image.0x4749"               --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeRatingNamespaces" --group "Iptc.Application2.Urgency"       --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeRatingNamespaces" --group "Xmp.MicrosoftPhoto.Rating"       --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeRatingNamespaces" --group "Xmp.acdsee.rating"               --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeRatingNamespaces" --group "Xmp.xmp.Rating"                  --key "isDisabled" false
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeTagsNamespaces" --group "Exif.Image.XPKeywords"             --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeTagsNamespaces" --group "Iptc.Application2.Keywords"        --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeTagsNamespaces" --group "Xmp.MicrosoftPhoto.LastKeywordXMP" --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeTagsNamespaces" --group "Xmp.acdsee.categories"             --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeTagsNamespaces" --group "Xmp.dc.subject"                    --key "isDisabled" false
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeTagsNamespaces" --group "Xmp.digiKam.TagsList"              --key "isDisabled" true
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeTagsNamespaces" --group "Xmp.lr.hierarchicalSubject"        --key "isDisabled" false
kwriteconfig5 --file digikamrc --type "bool" --group "DMetadata Settings" --group "writeTagsNamespaces" --group "Xmp.mediapro.CatalogSets"          --key "isDisabled" true


kwriteconfig5 --file digikamrc --type "bool" --group "ImageViewer Settings" --key "SlideShowStartCurrent" true

kwriteconfig5 --file digikamrc --type "bool" --group "Album Settings" --key "Icon Show Coordinates" true


kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "EXIF Rotate" true
kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "EXIF Set Orientation" true
kwriteconfig5 --file digikamrc --type "integer" --group "Metadata Settings" --key "Metadata Writing Mode" 3
kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "Rescan File If Modified" true
kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "Rotate By Internal Flag" true
kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "Rotate By Metadata Flag" true
kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "Rotate Contents Lossless" false
kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "Rotate Contents Lossy" false
kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "Save Color Label" true
kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "Save Date Time" true
kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "Save EXIF Comments" true
kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "Save FaceTags" true
kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "Save Pick Label" true
kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "Save Rating" true
kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "Save Tags" true
kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "Save Template" true
kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "Update File Timestamp" true
kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "Use Lazy Synchronization" false
kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "Use XMP Sidecar For Reading" true
kwriteconfig5 --file digikamrc --type "bool" --group "Metadata Settings" --key "Write RAW Files" false

echo "done."

# Desktop

echo -n "Applying kde config..."

kwriteconfig5 --file klaunchrc --type "bool" --group "BusyCursorSettings" --key "Blinking" false
kwriteconfig5 --file klaunchrc --type "bool" --group "BusyCursorSettings" --key "Bouncing" false
kwriteconfig5 --file klaunchrc --type "bool" --group "FeedbackStyle" --key "BusyCursor" false
kwriteconfig5 --file klaunchrc --type "bool" --group "FeedbackStyle" --key "TaskbarButton" true


kwriteconfig5 --file ksmserverrc --type "bool"   --group "General" --key "confirmLogout" false
kwriteconfig5 --file ksmserverrc --type "string" --group "General" --key "loginMode" "restoreSavedSession"

kwriteconfig5 --file kscreenlockerrc --type "bool" --group "Daemon" --key "Autolock" false

kwriteconfig5 --file krfbrc --type "bool" --group "Security" --key "allowUnattendedAccess" true
kwriteconfig5 --file krfbrc --type "bool" --group "TCP" --key "publishService" false

echo "done."
