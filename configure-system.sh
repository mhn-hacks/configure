#!/bin/bash -e


# Apt

echo -n "Applying apt config..."

cat > /etc/apt/apt.conf.d/99norecommend << EOF
APT::Install-Recommends "0";
APT::Install-Suggests "0";
EOF

cat > /etc/apt/sources.list << EOF
deb http://ftp.se.debian.org/debian/ stable main contrib non-free

deb http://ftp.se.debian.org/debian/ stretch main contrib non-free

deb http://security.debian.org/debian-security stretch/updates main contrib non-free

deb http://ftp.se.debian.org/debian/ experimental main contrib non-free
EOF

echo "done."


aptitude update
aptitude upgrade

aptitude install $(cat packages-whitelist.txt)
apt-mark manual  $(cat packages-whitelist.txt)

apt-mark showmanual | sort > installed-packages.txt

removable=$(comm -13 packages-whitelist.txt installed-packages.txt)
[ -z "$removable" ] || apt-mark auto $removable

aptitude install

aptitude remove $(cat packages-blacklist.txt)


replace_or_insert_line ()
{
  file=$1
  pattern=$2
  line=$3

  grep -qE "$pattern" "$file" && sed -ri "s/$pattern/$line/" $file || echo "$line" >> "$file"
}

replace_or_insert_line '/etc/vim/vimrc' '^[^[:alnum:]]*syntax [[:alpha:]]+$' 'syntax on'

replace_or_insert_line '/etc/inputrc' '^[^[:alnum:]]*set bell-style none$'         'set bell-style none'
replace_or_insert_line '/etc/inputrc' '^[^[:alnum:]]*set show-all-if-ambiguous on' 'set show-all-if-ambiguous on'

replace_or_insert_line '/etc/default/grub' '^[^[:alnum:]]*GRUB_DISABLE_LINUX_UUID=[[:alpha:]]*$' 'GRUB_DISABLE_LINUX_UUID=true'


#grep -E '^[^[:alnum:]]*syntax [[:alpha:]]+$' /etc/vim/vimrc && sed -ri 's/^[^[:alnum:]]*syntax [[:alpha:]]+$/syntax on/' /etc/vim/vimrc || echo "syntax on" > /etc/vim/vimrc
#grep -E '^[^[:alnum:]]*set bell-style none$' /etc/inputrc && sed -ri 's/^[^[:alnum:]]*set bell-style none$/set bell-style none/' /etc/inputrc || echo "set bell-style none" > /etc/inputrc
